## Intent

This issue describes how broader group and team members are facilitating a Product Designer to take meaningful time away from work. The intent is that the designer isn't required to "catch-up" before and after [taking a well-deserved vacation][time-off]. 

## :handshake: Responsibilities

List the priorities that the responsible backups need to cover. Backups can be another product designer, your design manager, or product manager.

Responsibilities include reviewing merge requests, responding to UX related questions or providing feedback related to inflight development work, responding to Slack inquiries. [See all responsibilities in the Handbook][responsibilities]

| Priority | Responsibility | Context Link | Primary backup DRI | Secondary backup DRI |
| ----------------- | -------------------- | ------------------ | ----------- | --------- |
| `HIGH/MEDIUM/LOW` | `e.g. Reviewing merge requests` | `ADD LINK` | `@assignee` | `@assignee` | 

<!--- 

Optionally, you can use the following priority badges:

- HIGH: [![Priority badge](https://img.shields.io/badge/Priority-High-red.svg)](https://shields.io/)
- MEDIUM: [![Priority badge](https://img.shields.io/badge/Priority-Medium-orange.svg)](https://shields.io/)
- LOW: [![Priority badge](https://img.shields.io/badge/Priority-Low-green.svg)](https://shields.io/)

--->

## :muscle: Coverage Tasks

Include specific issue links for tasks to be completed.

- [ ] `Item` - `Link` - `@assignee`

## 🚙 Parked tasks

Larger design responsibilities that aren't moving forward until I return, such as creating a prototype or planning/conducting research.

- [ ] `Item` - `Link`

## :book: References

Here are some references for how I and my group generally works:

- [Group Process Handbook Page](ADD LINK)
- [Group UX Vision Handbook Page](ADD LINK)
- [Key direction pages](ADD LINK)
- [Additional References](ADD LINK)

## :white_check_mark: Issue Tasks

### :o: Opening Tasks

- [ ] Assign to yourself
- [ ] Title the issue `UX Coverage for YOUR-NAME from XXXX-XX-XX until XXXX-XX-XX`
- [ ] Add an issue comment for your retrospective titled: `:recycle: Retrospective Thread`
- [ ] Add any relevant references including direction pages, ux handbook pages, etc
- [ ] Fill in the Responsibilities table with broad based responsibilities
- [ ] Fill in the specific Coverage Tasks with distinct items to complete and assignees
- [ ] Assign to anyone with a specific task or responsibility assigned to them
- [ ] Share this issue in your section, stage and group Slack channels
- [ ] Ensure your PTO Ninja auto-responder points team members to this issue

### :x: Closing Tasks

- [ ] Assign back to yourself and remove others
- [ ] Review any Retrospective items with your counterparts and manager

<!-- Do not remove the items below -->

[responsibilities]: https://about.gitlab.com/job-families/engineering/product-designer/
[time-off]: https://about.gitlab.com/handbook/engineering/ux/how-we-work/#time-off
